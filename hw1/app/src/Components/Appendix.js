import React, { Component } from "react";
import FirstModal from "./FirstModal";
import SecondModal from "./SecondModal";
import "../style/styleButtons.scss";
import "../style/styleModal.scss";

class Appendix extends Component {
  state = {
    isOpenFirstModal: false,
    isOpenSecondModal: false,
  };

  render() {
    const firstModal = this.state.isOpenFirstModal && <FirstModal />;
    const secondModal = this.state.isOpenSecondModal && <SecondModal />;
    return (
      <div>
        <button
          className="double-border-button"
          onClick={this.handleClickFirstModal}
        >
          Open first modal
        </button>

        <button
          className="double-border-button"
          onClick={this.handleClickSecondModal}
        >
          Open second modal
        </button>
        {firstModal}
        {secondModal}
      </div>
    );
  }
  handleClickFirstModal = () => {
    this.setState({
      isOpenFirstModal: !this.state.isOpenFirstModal,
      isOpenSecondModal: false,
    });
  };

  handleClickSecondModal = () => {
    this.setState({
      isOpenSecondModal: !this.state.isOpenSecondModal,
      isOpenFirstModal: false,
    });
  };
}

export default Appendix;
