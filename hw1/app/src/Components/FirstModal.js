import React, { Component } from "react";

class FirstModal extends Component {
  state = {
    isOpenModal: true,
  };
  render() {
    const modal = this.state.isOpenModal && (
      <div className={`modal modal-content`}>
        <div className={"modal__content-header"}>
          <header className="modal-header">
            Do you want to delete this file?
          </header>
          <div className="modal__close " onClick={this.clickCloseModal}>
            X
          </div>
        </div>
        <p className="modal__content-main">
          Once you delete this file? it won`t be possible to undo this action.
        </p>
        <p className="modal__content-main">
          Are you sure you want to delete it?
        </p>
        <div className="modal-btn-flex">
          <button className="modal-buttton" onClick={this.clickCloseModal}>
            Ok
          </button>
          <button className="modal-buttton">Cancel</button>
        </div>
      </div>
    );
    return <div>{modal}</div>;
  }

  clickCloseModal = () => {
    this.setState({
      isOpenModal: !this.state.isOpenModal,
    });
  };
}
export default FirstModal;
