import React from "react";

function ButtonFirst(props) {
  const { button } = props;
  return (
    <div>
      <button>{button.text}</button>
    </div>
  );
}

export default ButtonFirst;
