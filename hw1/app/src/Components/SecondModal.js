import React, { Component } from "react";

class SecondModal extends Component {
  state = {
    isOpenModal: true,
  };
  render() {
    const modal = this.state.isOpenModal && (
      <div className={`modal modal-content`}>
        <div className={"modal__content-header"}>
          <header className="modal-header">Delete your Account?</header>
          <div className="modal__close " onClick={this.clickCloseModal}>
            X
          </div>
        </div>
        <p className="modal__content-main">
          This action is final and you will be unable to recover any data
        </p>
        <p className="modal__content-main">Are you confirming the action?</p>
        <div className="modal-btn-flex">
          <button  className="modal-buttton" onClick={this.clickCloseModal}>Yes</button>
          <button className="modal-buttton">No</button>
        </div>
      </div>
    );
    return <div>{modal}</div>;
  }

  clickCloseModal = () => {
    this.setState({
      isOpenModal: !this.state.isOpenModal,
    });
  };
}

export default SecondModal;
