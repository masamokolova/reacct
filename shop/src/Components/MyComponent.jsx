import React, { Component } from "react";
import ButtonFirst from "./ButtonFirst";
import Star from "./Star";

class MyComponent extends Component {
  render() {
    const { items } = this.props;

    return (
      <div className="notebook-block">
        <ul>
          <div className="flex-content">
            {items.map((item, index) => (
              <li key={index}>
                <div className="block-content">
                  <img
                    className="notebook-block__image"
                    src={item.image}
                    title={item.url}
                    alt="#"
                  ></img>
                  <div className="flex-content-block">
                    <h3 className="notebook-block__title">{item.title}</h3>
                    <div className="flexContent">
                      <h4>Цвет: {item.color}</h4> <Star />
                    </div>
                    <div className="flexContent">
                      <span className="notebook-price">{item.price}₴</span>
                      <ButtonFirst />
                    </div>
                  </div>
                </div>
              </li>
            ))}
          </div>
        </ul>
      </div>
    );
  }
}

export default MyComponent;
