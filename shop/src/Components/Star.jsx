import React, { Component } from "react";

class Star extends Component {
    constructor(props) {
      super(props);
    this.state = {
      flag: JSON.parse(localStorage.getItem("chandeColor")) || false,
     
    };
  }
  changeColor = () => {
    const flag = { ...this.state.flag };
    this.setState({
      flag: !this.state.flag,
    });
  
    localStorage.setItem("chandeColor", JSON.stringify(flag));
  };
  render(){
   
  return(
    <div className="starButton" style={{
        color: this.state.flag === true ? "orange" : "black",
      }}
      onClick={this.changeColor}> ★ </div>
  )

}}

export default Star;