import React, { Component } from "react";
import WarningModal from "./WarningModal";

class ButtonFirst extends Component {
  
  state = {
    isOpenFirstModal: false,
  };
   render() {
  const warningModal = this.state.isOpenFirstModal && <WarningModal />;
    const handleClickWarningModal = () => {
      this.setState({
        isOpenFirstModal: !this.state.isOpenFirstModal,
      });
    };
  return (
    <div>
    <button
     onClick={handleClickWarningModal}
      className="button button--outline button--add">
      Add to cart
    </button>
    {warningModal}
    </div>
  );
};
}
export default ButtonFirst;
