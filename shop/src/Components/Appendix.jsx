import React, { Component } from "react";
import Nav from "./Nav";
import MyComponent from "./MyComponent";
import { items } from "../sampleNotebook";

class Appendix extends Component {
  render() {
    return (
      <div className="wrapper">
        <Nav />
        <div className="container">
          <MyComponent items={items}> </MyComponent>
        </div>
      </div>
    );
  }
  
}

export default Appendix;
