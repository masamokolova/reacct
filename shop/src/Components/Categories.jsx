import React, { Component } from "react";

class Categories extends Component {
  state = {
    activeItem: 3,
  };

  onSelectorItem = (index) => {
    this.setState({
      activeItem: index,
    });
  };
  render() {
    const { items } = this.props;
    return (
      <div className="categories">
        <ul>
          {items.map((name, index) => (
            <li
              className={this.state.activeItem === index ? "active" : " "}
              onClick={() => this.onSelectorItem(index)}
              key={`${name}_${index}`}
            >
              {name}
            </li>
          ))}
        </ul>
      </div>
    );
  }
}
export default Categories;
