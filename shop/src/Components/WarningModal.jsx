import React, { Component } from "react";

class WarningModal extends Component {
  state = {
    isOpenModal: true,
  };
  render() {
    const modal = this.state.isOpenModal && (
      <div className={`modal modal-content`}>
        <div className={"modal__content-header"}>
          <header className="modal-header">Warning</header>
          <div className="modal__close " onClick={this.clickCloseModal}>
            X
          </div>
        </div>
        <p className="modal__content-main">Add item to cart?</p>
        <div className="modal-btn-flex">
          <button className="modal-buttton" onClick={this.clickCloseModal}>
            Yes
          </button>
          <button className="modal-buttton">No</button>
        </div>
      </div>
    );
    return <div>{modal}</div>;
  }

  clickCloseModal = () => {
    this.setState({
      isOpenModal: !this.state.isOpenModal,
    });
  };
}

export default WarningModal;
