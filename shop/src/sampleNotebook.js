export const items = [
  {
    title: "Ноутбук Asus VivoBook 15",
    price: 19499,
    url: "notebook-asus-vivoBook_15",
    color: "gray",
    image: "/img/asus_vivoBook15.jpg",
  },

  {
    title: "Ноутбук Lenovo IdeaPad 3",
    price: 18999,
    url: "notebook-lenovo-ideaPad_3",
    color: "gray",
    image: "/img/lenovo_ideaPad_3.jpg",
  },

  {
    title: "Ноутбук HP Laptop 15s",
    price: 18399,
    url: "notebook-hp-laptop_15s",
    color: "white",
    image: "/img/hp-laptop_15s.jpg",
  },

  {
    title: "Ноутбук Apple MacBook 13",
    price: 33999,
    url: "notebook-apple-macBook_13",
    color: "gold",
    image: "/img/apple_macbook_air_13.jpg",
  },

  {
    title: "Ноутбук ACER Aspire 3",
    price: 15999,
    url: "notebook-ACER-Aspire_3",
    color: "black",
    image: "/img/acer-aspire_3.jpg",
  },

  {
    title: "Ноутбук Samsung Galaxy",
    price: 591518,
    url: "notebook-sansung-galaxy",
    color: "white",
    image: "/img/Galaxy-Book-Ion2.jpg",
  },

  {
    title: "Ноутбук Mi RedmiBook 14",
    price: 26999,
    url: "notebook-mi-redmiBook_14",
    color: "gray",
    image: "/img/mi-redmi_14.jpg",
  },

  {
    title: "Ноутбук Microsoft Surface",
    price: 30499,
    url: "notebook-Microsoft-Surface",
    color: "gold",
    image: "/img/microsoft.jpg",
  },

  {
    title: "Ноутбук Huawei MateBook 15",
    price: 323999,
    url: "notebook-Huawei-MateBook_15",
    color: "black",
    image: "/img/huawei-mateBook-d15.jpg",
  },

  {
    title: "Ноутбук Dell Vostro 15",
    price: 21319,
    url: "notebook-Dell-Vostro_15",
    color: "black",
    image: "/img/dell-g3.jpg",
  },

  {
    title: "Ноутбук MSI Modern 14",
    price: 20999,
    url: "notebook-MSI-Modern_14",
    color: "gray",
    image: "/img/msi-modern-14.jpg",
  },

  {
    title: "Ноутбук RAZER PRO 17",
    price: 71611,
    url: "notebook-razer-pro_17",
    color: "black",
    image: "/img/razer.jpg",
  },
];

// [{"id": 1,"title": "Ноутбук Asus VivoBook 15", "image": "/img/lenovo_ideaPad_3.jpg"},
// {"id": 2, "title": "Last Time I Committed Suicide, The"},
// {"id": 3, "title": "Jerry Seinfeld: 'I'm Telling You for the Last Time'"},
// {"id": 4, "title": "Youth Without Youth"},
// {"id": 5, "title": "Happy Here and Now"},
// {"id": 6, "title": "Wedding in Blood (Noces rouges, Les)"},
// {"id": 7, "title": "Vampire in Venice (Nosferatu a Venezia) (Nosferatu in Venice)"},
// {"id": 8, "title": "Monty Python The Meaning of Life"},
// {"id": 9, "title": "Awakening, The"},
// {"id": 10, "title": "Trip, The"}]
